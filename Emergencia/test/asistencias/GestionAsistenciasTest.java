package asistencias;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import exceptions.GestionAsistenciasException;
import asistencias.AsistenciaMedica;
import asistencias.GestionAsistencias;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class GestionAsistenciasTest {

    @Test
    public void testAltaAsistenciaMedica() {
        GestionAsistencias gestionAsistencias = new GestionAsistencias();
        AsistenciaMedica asistenciaMedica = new AsistenciaMedica(null,null,null,null,null,null,null,null,null,null,null,null);
        try {
            gestionAsistencias.altaAsistenciaMedica(asistenciaMedica);
        } catch (GestionAsistenciasException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionAsistencias.getAsistenciasMedicas().contains(asistenciaMedica));
    }

    @Test
    public void testBajaAsistenciaMedica() {
        GestionAsistencias gestionAsistencias = new GestionAsistencias();
        AsistenciaMedica asistenciaMedica = new AsistenciaMedica(null,null,null,null,null,null,null,null,null,null,null,null);
        gestionAsistencias.getAsistenciasMedicas().add(asistenciaMedica);
        try {
            gestionAsistencias.bajaAsistenciaMedica(asistenciaMedica.getNumeroDeOperacion());
        } catch (GestionAsistenciasException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionAsistencias.getAsistenciasMedicas().contains(asistenciaMedica));
    }

    @Test
    public void testBuscarPorNumeroDeOperacion() {
        GestionAsistencias gestionAsistencias = new GestionAsistencias();
        AsistenciaMedica asistenciaMedica = new AsistenciaMedica(null,null,null,null,null,null,null,null,null,null,null,null);
        gestionAsistencias.getAsistenciasMedicas().add(asistenciaMedica);
        try {
            AsistenciaMedica encontrada = gestionAsistencias.buscarPorNumeroDeOperacion(asistenciaMedica.getNumeroDeOperacion());
            assertEquals(asistenciaMedica, encontrada);
        } catch (GestionAsistenciasException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}
