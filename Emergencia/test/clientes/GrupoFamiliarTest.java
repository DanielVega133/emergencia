/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package clientes;

import exceptions.GestionFamiliarException;
import exceptions.GestionAfiliadoException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 *
 * @author Daniel
 */
public class GrupoFamiliarTest {
    
    
    

    @Test
    public void testAltaAfiliado() {
        GrupoFamiliar grupoFamiliar = new GrupoFamiliar();
        Afiliado afiliado = new Afiliado("a","a",true,"a",21,"a","a","a");
        try {
            grupoFamiliar.altaAfiliado(afiliado);
        } catch (GestionAfiliadoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(grupoFamiliar.getAfiliados().contains(afiliado));
    }

    @Test
    public void testBajaAfiliado() {
        GrupoFamiliar grupoFamiliar = new GrupoFamiliar();
        Afiliado afiliado = new Afiliado("a","a",true,"a",21,"a","a","a");
        grupoFamiliar.getAfiliados().add(afiliado);
        try {
            grupoFamiliar.bajaAfiliado(afiliado.getNumeroDeAfiliado());
        } catch (GestionAfiliadoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(grupoFamiliar.getAfiliados().contains(afiliado));
    }

    @Test
    public void testBuscarPorNumeroDeAfiliado() {
        GrupoFamiliar grupoFamiliar = new GrupoFamiliar();
        Afiliado afiliado = new Afiliado("a","a",true,"a",21,"a","a","a");
        grupoFamiliar.getAfiliados().add(afiliado);
        try {
            Afiliado encontrado = grupoFamiliar.buscarPorNumeroDeAfiliado(afiliado.getNumeroDeAfiliado());
            assertEquals(afiliado, encontrado);
        } catch (GestionAfiliadoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }

    
}
