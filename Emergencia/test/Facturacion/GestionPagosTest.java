/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import exceptions.GestionFacturaException;
import facturacion.FacturaPago;
import facturacion.GestionPagos;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class GestionPagosTest {

    @Test
    public void testAltaFacturaPago() {
        GestionPagos gestionPagos = new GestionPagos();
        FacturaPago facturaPago = new FacturaPago(null,null);
        try {
            gestionPagos.altaFacturaPago(facturaPago);
        } catch (GestionFacturaException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionPagos.getFacturasPago().contains(facturaPago));
    }

    @Test
    public void testBajaFacturaPago() {
        GestionPagos gestionPagos = new GestionPagos();
        FacturaPago facturaPago = new FacturaPago(null,null);
        gestionPagos.getFacturasPago().add(facturaPago);
        try {
            gestionPagos.bajaFacturaPago(facturaPago.getNumeroDeFactura());
        } catch (GestionFacturaException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionPagos.getFacturasPago().contains(facturaPago));
    }
    @Test
    public void testBuscarPorNumeroDeFactura() {
        GestionPagos gestionPagos = new GestionPagos();
        FacturaPago facturaPago = new FacturaPago(null,null);
        gestionPagos.getFacturasPago().add(facturaPago);
        try {
            FacturaPago encontrado = gestionPagos.buscarPorNumeroDeFactura(facturaPago.getNumeroDeFactura());
            assertEquals(facturaPago, encontrado);
        } catch (GestionFacturaException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}