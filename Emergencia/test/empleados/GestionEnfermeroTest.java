package empleados;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import empleados.Enfermero;
import empleados.GestionEnfermero;
import exceptions.GestionEnfermeroException;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class GestionEnfermeroTest {

    @Test
    public void testAltaEnfermero() {
        GestionEnfermero gestionEnfermero = new GestionEnfermero();
        Enfermero enfermero = new Enfermero("a","a",false,1,"a","a","a");
        try {
            gestionEnfermero.altaEnfermero(enfermero);
        } catch (GestionEnfermeroException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionEnfermero.getEnfermeros().contains(enfermero));
    }

    @Test
    public void testBajaEnfermero() {
        GestionEnfermero gestionEnfermero = new GestionEnfermero();
        Enfermero enfermero = new Enfermero("a","a",false,1,"a","a","a");
        gestionEnfermero.getEnfermeros().add(enfermero);
        try {
            gestionEnfermero.bajaEnfermero(enfermero.getIdentificacionSanitaria());
        } catch (GestionEnfermeroException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionEnfermero.getEnfermeros().contains(enfermero));
    }

    @Test
    public void testBuscarPorMatriculaDeEnfermero() {
        GestionEnfermero gestionEnfermero = new GestionEnfermero();
        Enfermero enfermero = new Enfermero("a","a",false,1,"a","a","a");
        gestionEnfermero.getEnfermeros().add(enfermero);
        try {
            Enfermero encontrado = gestionEnfermero.buscarPorIdentificacionSanitaria(enfermero.getIdentificacionSanitaria());
            assertEquals(enfermero, encontrado);
        } catch (GestionEnfermeroException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}