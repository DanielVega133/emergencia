/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

/**
 *
 * @author Daniel
 */
import empleados.Doctor;
import empleados.GestionDoctor;
import exceptions.GestionDoctorException;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class GestionDoctorTest {

    @Test
    public void testAltaDoctor() {
        GestionDoctor gestionDoctor = new GestionDoctor();
        Doctor doctor = new Doctor("a","a","a",false,1,"a","a","a");
        try {
            gestionDoctor.altaDoctor(doctor);
        } catch (GestionDoctorException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionDoctor.getDoctores().contains(doctor));
    }

    @Test
    public void testBajaDoctor() {
        GestionDoctor gestionDoctor = new GestionDoctor();
        Doctor doctor = new Doctor("a","a","a",false,1,"a","a","a");
        gestionDoctor.getDoctores().add(doctor);
        try {
            gestionDoctor.bajaDoctor(doctor.getNumeroLicenciaMedica());
        } catch (GestionDoctorException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionDoctor.getDoctores().contains(doctor));
    }

    @Test
    public void testBuscarPorMatriculaDeDoctor() {
        GestionDoctor gestionDoctor = new GestionDoctor();
        Doctor doctor = new Doctor("a","a","a",false,1,"a","a","a");
        gestionDoctor.getDoctores().add(doctor);
        try {
            Doctor encontrado = gestionDoctor.buscarPorNumeroLicenciaMedica(doctor.getNumeroLicenciaMedica());
            assertEquals(doctor, encontrado);
        } catch (GestionDoctorException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}

