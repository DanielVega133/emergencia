package empleados;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import empleados.Chofer;
import empleados.GestionChofer;
import exceptions.GestionChoferException;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class GestionChoferTest {

    @Test
    public void testAltaChofer() {
        GestionChofer gestionChofer = new GestionChofer();
        Chofer chofer = new Chofer("a","a",false,1,"a","a","a");
        try {
            gestionChofer.altaChofer(chofer);
        } catch (GestionChoferException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionChofer.getChoferes().contains(chofer));
    }

    @Test
    public void testBajaChofer() {
        GestionChofer gestionChofer = new GestionChofer();
        Chofer chofer = new Chofer("a","a",false,1,"a","a","a");
        gestionChofer.getChoferes().add(chofer);
        try {
            gestionChofer.bajaChofer(chofer.getNumeroDeCarnet());
        } catch (GestionChoferException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionChofer.getChoferes().contains(chofer));
    }

    @Test
    public void testBuscarPorIdChofer() {
        GestionChofer gestionChofer = new GestionChofer();
        Chofer chofer = new Chofer("a","a",false,1,"a","a","a");
        gestionChofer.getChoferes().add(chofer);
        try {
            Chofer encontrado = gestionChofer.buscarPorCarnet(chofer.getNumeroDeCarnet());
            assertEquals(chofer, encontrado);
        } catch (GestionChoferException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}
