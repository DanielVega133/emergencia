/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import empleados.Administrativo;
import empleados.GestionAdministrativos;
import exceptions.GestionAdministrativoException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;
/**
 *
 * @author Daniel
 */
public class GestionAdministrativosTest {
    
    @Test
    public void testAltaAdministrativo() {
        GestionAdministrativos gestionAdministrativos = new GestionAdministrativos();
        Administrativo administrativo = new Administrativo("a","a",false,1,"a","a","a");
        try {
            gestionAdministrativos.altaAdministrativo(administrativo);
        } catch (GestionAdministrativoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(gestionAdministrativos.getAdministrativo().contains(administrativo));
    }

    @Test
    public void testBajaAdmin() {
        GestionAdministrativos gestionAdministrativos = new GestionAdministrativos();
        Administrativo administrativo = new Administrativo("a","a",false,1,"a","a","a");
        gestionAdministrativos.getAdministrativo().add(administrativo);
        try {
            gestionAdministrativos.bajaAdmin(administrativo.getNumeroDeEmpleadoAdministrativo());
        } catch (GestionAdministrativoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(gestionAdministrativos.getAdministrativo().contains(administrativo));
    }

    @Test
    public void testBuscarPorNumeroDeEmpleado() {
        GestionAdministrativos gestionAdministrativos = new GestionAdministrativos();
        Administrativo administrativo = new Administrativo("a","a",false,1,"a","a","a");
        gestionAdministrativos.getAdministrativo().add(administrativo);
        try {
            Administrativo encontrado = gestionAdministrativos.buscarPorNumeroDeEmpleado(administrativo.getNumeroDeEmpleadoAdministrativo());
            assertEquals(administrativo, encontrado);
        } catch (GestionAdministrativoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}

