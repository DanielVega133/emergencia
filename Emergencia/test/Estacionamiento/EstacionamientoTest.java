package Estacionamiento;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import estacionamiento.Auto;
import estacionamiento.Estacionamiento;
import exceptions.GestionAutoException;
import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;

public class EstacionamientoTest {

    @Test
    public void testAltaAuto() {
        Estacionamiento estacionamiento = new Estacionamiento();
        Auto auto = new Auto("a","a",false);
        try {
            estacionamiento.altaAuto(auto);
        } catch (GestionAutoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertTrue(estacionamiento.getAutos().contains(auto));
    }

    @Test
    public void testBajaAuto() {
        Estacionamiento estacionamiento = new Estacionamiento();
        Auto auto = new Auto("a","a",false);
        estacionamiento.getAutos().add(auto);
        try {
            estacionamiento.bajaAuto(auto.getMatricula());
        } catch (GestionAutoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
        assertFalse(estacionamiento.getAutos().contains(auto));
    }

    @Test
    public void testBuscarPorMatricula() {
        Estacionamiento estacionamiento = new Estacionamiento();
        Auto auto = new Auto("a","a",false);
        estacionamiento.getAutos().add(auto);
        try {
            Auto encontrado = estacionamiento.buscarPorMatricula(auto.getMatricula());
            assertEquals(auto, encontrado);
        } catch (GestionAutoException e) {
            fail("Excepción inesperada: " + e.getMessage());
        }
    }
}
