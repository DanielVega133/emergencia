/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package asistencias;
import exceptions.GestionAsistenciasException;
import java.util.ArrayList; 
/**
 *
 * @author Daniel
 */
public class GestionAsistencias implements GestionAsistenciasInterfaz{
        private ArrayList<AsistenciaMedica> asistenciaMedicaList = new ArrayList<AsistenciaMedica>();
    
    public void altaAsistenciaMedica(AsistenciaMedica asistenciaMedica) throws GestionAsistenciasException{
        boolean band = false;
        for(AsistenciaMedica asis : asistenciaMedicaList){
            if(asis.getNumeroDeOperacion().equals(asistenciaMedica.getNumeroDeOperacion()))
            {
                band=true;
            }   
        }
        if(!band){
            asistenciaMedicaList.add(asistenciaMedica);
        } else {
            throw new GestionAsistenciasException("Asistencia Médica existente");
        }
    }
    
    public ArrayList<AsistenciaMedica> getAsistenciasMedicas() {
        return asistenciaMedicaList;
    }

    public void bajaAsistenciaMedica(String numeroDeOperacion) throws GestionAsistenciasException{
        Boolean band = false;
        for(AsistenciaMedica asis : asistenciaMedicaList){
             if (asis.getNumeroDeOperacion().equals(numeroDeOperacion)){
                 asistenciaMedicaList.remove(asis);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionAsistenciasException("No se encontró la Asistencia Médica");
         }
    }

    public AsistenciaMedica buscarPorNumeroDeOperacion(String numero) throws GestionAsistenciasException {
        for (AsistenciaMedica asis : asistenciaMedicaList) {
            if (asis.getNumeroDeOperacion().equals(numero)) {
                return asis;
            }
        }
        throw new GestionAsistenciasException("No se encontró la Asistencia Médica");
    }
}

