/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package asistencias;

import exceptions.GestionAsistenciasException; 
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionAsistenciasInterfaz {
    public void altaAsistenciaMedica(AsistenciaMedica asistenciaMedica) throws GestionAsistenciasException;    
    public ArrayList<AsistenciaMedica> getAsistenciasMedicas();
    public void bajaAsistenciaMedica(String numeroDeOperacion) throws GestionAsistenciasException;
    public AsistenciaMedica buscarPorNumeroDeOperacion(String numero) throws GestionAsistenciasException ;
}
