/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package asistencias;

import clientes.Afiliado;
import clientes.Familiar;
import empleados.Doctor;
import empleados.Chofer;
import empleados.Administrativo;
import empleados.Enfermero;
import estacionamiento.Auto;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author Danie
 */
public class AsistenciaMedica {
   private Administrativo administrativoQueAtendio;
   private Chofer choferAsignado;
   private Doctor doctorAsignado;
   private Enfermero enfermeroAsignado;
   private Afiliado afiliadoACargo;
   private Familiar familiarAsistido;
   private Auto ambulancia;
   private Boolean asistenciaAFamiliar;
   private LocalDate fechaDeEmisionDeAsistencia;
   private LocalTime horaDeSalida;
   private String numeroDeOperacion;
   private String descripcion;

    public AsistenciaMedica(Administrativo administrativoQueAtendio, Chofer choferAsignado, Doctor doctorAsignado, Enfermero enfermeroAsignado, Afiliado afiliadoACargo, Familiar familiarAsistido, Auto ambulancia, Boolean asistenciaAFamiliar, LocalDate fechaDeEmisionDeAsistencia, LocalTime horaDeSalida, String numeroDeOperacion, String descripcion) {
        this.administrativoQueAtendio = administrativoQueAtendio;
        this.choferAsignado = choferAsignado;
        this.doctorAsignado = doctorAsignado;
        this.enfermeroAsignado = enfermeroAsignado;
        this.afiliadoACargo = afiliadoACargo;
        this.familiarAsistido = familiarAsistido;
        this.ambulancia = ambulancia;
        this.asistenciaAFamiliar = asistenciaAFamiliar;
        this.fechaDeEmisionDeAsistencia = fechaDeEmisionDeAsistencia;
        this.horaDeSalida = horaDeSalida;
        this.numeroDeOperacion = numeroDeOperacion;
        this.descripcion = descripcion;
    }

    public Auto getAmbulancia() {
        return ambulancia;
    }

    public void setAmbulancia(Auto ambulancia) {
        this.ambulancia = ambulancia;
    }


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public String getNumeroDeOperacion() {
        return numeroDeOperacion;
    }

    public void setNumeroDeOperacion(String numeroDeOperacion) {
        this.numeroDeOperacion = numeroDeOperacion;
    }
   
   
    public Administrativo getAdministrativoQueAtendio() {
        return administrativoQueAtendio;
    }

    public void setAdministrativoQueAtendio(Administrativo administrativoQueAtendio) {
        this.administrativoQueAtendio = administrativoQueAtendio;
    }

    public Chofer getChoferAsignado() {
        return choferAsignado;
    }

    public void setChoferAsignado(Chofer choferAsignado) {
        this.choferAsignado = choferAsignado;
    }

    public Doctor getDoctorAsignado() {
        return doctorAsignado;
    }

    public void setDoctorAsignado(Doctor doctorAsignado) {
        this.doctorAsignado = doctorAsignado;
    }

    public Enfermero getEnfermeroAsignado() {
        return enfermeroAsignado;
    }

    public void setEnfermeroAsignado(Enfermero enfermeroAsignado) {
        this.enfermeroAsignado = enfermeroAsignado;
    }

    public Afiliado getAfiliadoACargo() {
        return afiliadoACargo;
    }

    public void setAfiliadoACargo(Afiliado afiliadoACargo) {
        this.afiliadoACargo = afiliadoACargo;
    }

    public Boolean getAsistenciaAFamiliar() {
        return asistenciaAFamiliar;
    }

    public void setAsistenciaAFamiliar(Boolean asistenciaAFamiliar) {
        this.asistenciaAFamiliar = asistenciaAFamiliar;
    }

    public LocalDate getFechaDeEmisionDeAsistencia() {
        return fechaDeEmisionDeAsistencia;
    }

    public void setFechaDeEmisionDeAsistencia(LocalDate fechaDeEmisionDeAsistencia) {
        this.fechaDeEmisionDeAsistencia = fechaDeEmisionDeAsistencia;
    }

    public LocalTime getHoraDeSalida() {
        return horaDeSalida;
    }

    public Familiar getFamiliarAsistido() {
        return familiarAsistido;
    }

    public void setFamiliarAsistido(Familiar familiarAsistido) {
        this.familiarAsistido = familiarAsistido;
    }

    
    public void setHoraDeSalida(LocalTime horaDeSalida) {
        this.horaDeSalida = horaDeSalida;
    }
   
    public boolean equals(Object asistencia1) {
        if (this == asistencia1) {
            return true;
        }
        if (asistencia1 == null) {
            return false;
        }
        if (getClass() != asistencia1.getClass()) {
            return false;
        }
        AsistenciaMedica other = (AsistenciaMedica) asistencia1;
        if (other.getAfiliadoACargo().equals(this.afiliadoACargo) && other.getDoctorAsignado().equals(this.doctorAsignado) && other.getNumeroDeOperacion().equals(this.numeroDeOperacion)) {
            return true;
        }else{
            return false;
        }
    }
    
}
