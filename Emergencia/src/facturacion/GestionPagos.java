/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package facturacion;

import exceptions.GestionFacturaException;
import java.util.ArrayList; 
/**
 *
 * @author Daniel
 */
public class GestionPagos implements GestionPagosInterfaz{
    private ArrayList<FacturaPago> facturaPagoList = new ArrayList<FacturaPago>();
    
    public void altaFacturaPago(FacturaPago facturaPago) throws GestionFacturaException{
        for(FacturaPago factura : facturaPagoList){
            if(factura.getNumeroDeFactura().equals(facturaPago.getNumeroDeFactura()))
            {
                throw new GestionFacturaException("Factura de Pago existente");
            }   
        }
        facturaPagoList.add(facturaPago);
    }
    
    public ArrayList<FacturaPago> getFacturasPago() {
        return facturaPagoList;
    }

    public void bajaFacturaPago(String numeroDeFactura) throws GestionFacturaException{
        FacturaPago facturaEncontrada = null;
        for(FacturaPago factura : facturaPagoList){
            if (factura.getNumeroDeFactura().equals(numeroDeFactura)){
                facturaEncontrada = factura;
                break;
            }
        }
        if(facturaEncontrada != null){
            facturaPagoList.remove(facturaEncontrada);
        } else {
            throw new GestionFacturaException("No se encontró la Factura de Pago");
        }
    }

    public FacturaPago buscarPorNumeroDeFactura(String numero) throws GestionFacturaException {
        for (FacturaPago factura : facturaPagoList) {
            if (factura.getNumeroDeFactura().equals(numero)) {
                return factura;
            }
        }
        throw new GestionFacturaException("No se encontró la Factura de Pago");
    }
}
