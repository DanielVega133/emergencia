/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package facturacion;

import exceptions.GestionFacturaException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionPagosInterfaz {
     public void altaFacturaPago(FacturaPago facturaPago) throws GestionFacturaException;
    
    public ArrayList<FacturaPago> getFacturasPago();

    public void bajaFacturaPago(String numeroDeFactura) throws GestionFacturaException;

    public FacturaPago buscarPorNumeroDeFactura(String numero) throws GestionFacturaException;
}
