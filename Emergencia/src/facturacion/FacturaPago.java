/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package facturacion;

import asistencias.AsistenciaMedica;
import clientes.Afiliado;
import empleados.Administrativo;
import exceptions.GestionElementoFacturaException;
import java.util.ArrayList;

/**
 *
 * @author Danie
 */
public class FacturaPago {
    private Afiliado afiliadoEspecifico;
    private String numeroDeFactura;
    private ArrayList<ElementoFactura> elementosList = new ArrayList();

    public FacturaPago(Afiliado afiliadoEspecifico, String numeroDeFactura) {
        this.afiliadoEspecifico = afiliadoEspecifico;
        this.numeroDeFactura = numeroDeFactura;
    }
    
    

    public String getNumeroDeFactura() {
        return numeroDeFactura;
    }

    public void setNumeroDeFactura(String numeroDeFactura) {
        this.numeroDeFactura = numeroDeFactura;
    }

    

    public Afiliado getAfiliadoEspecifico() {
        return afiliadoEspecifico;
    }

    public void setAfiliadoEspecifico(Afiliado afiliadoEspecifico) {
        this.afiliadoEspecifico = afiliadoEspecifico;
    }

    public void altaElementoFactura(ElementoFactura elemento) throws GestionElementoFacturaException {
        boolean band = false;
        for (ElementoFactura e : elementosList) {
            if (e.getCodigo().equals(elemento.getCodigo())) {
                band = true;
                break;
            }
        }
        if (!band) {
            elementosList.add(elemento);
        } else {
            throw new GestionElementoFacturaException("Elemento de factura existente");
        }
    }

    public Float calculoTotal(){
        float montoFinal= 0.0f;
        for(ElementoFactura e : elementosList){
            montoFinal=montoFinal+e.getPrecio();
        }
        return montoFinal;
    }
    
    public ArrayList<ElementoFactura> getElementosFactura() {
        return elementosList;
    }

    public void bajaElementoFactura(String codigo) throws GestionElementoFacturaException {
        boolean band = false;
        ElementoFactura elementoToRemove = null;
        for (ElementoFactura e : elementosList) {
            if (e.getCodigo().equals(codigo)) {
                elementoToRemove = e;
                band = true;
                break;
            }
        }
        if (band) {
            elementosList.remove(elementoToRemove);
        } else {
            throw new GestionElementoFacturaException("No se encontró el elemento de factura");
        }
    }

    public ElementoFactura buscarPorCodigo(String codigo) throws GestionElementoFacturaException {
        for (ElementoFactura e : elementosList) {
            if (e.getCodigo().equals(codigo)) {
                return e;
            }
        }
        throw new GestionElementoFacturaException("No se encontró el elemento de factura");
    }
    

    public boolean equals(Object factura) {
        //optimizacion
        if (this == factura) {
            return true;
        }
        //no null
        if (factura == null) {
            return false;
        }
        //la clase es igual
        if (getClass() != factura.getClass()) {
            return false;
        }
        //comparacion campo a campo
        FacturaPago other = (FacturaPago) factura;
        if (other.getAfiliadoEspecifico().equals(this.afiliadoEspecifico)) {
            return true;
        }else{
            return false;
        }

    }
    
    
    
}
