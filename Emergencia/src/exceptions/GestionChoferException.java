/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exceptions;

/**
 *
 * @author Daniel
 */
public class GestionChoferException extends Exception {
    
    public GestionChoferException(String mensaje) {
       super(mensaje);
    }
}