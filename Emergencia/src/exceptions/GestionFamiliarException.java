/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exceptions;

/**
 *
 * @author Daniel
 */
public class GestionFamiliarException extends Exception{
    
    public GestionFamiliarException(String mensaje) {
       super(mensaje);
    }
}