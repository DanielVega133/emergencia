/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package exceptions;

/**
 *
 * @author Daniel
 */
public class GestionAutoException extends Exception {
    
    public GestionAutoException(String mensaje) {
       super(mensaje);
    }
}