/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emergencia;
import clientes.GrupoFamiliar;
import empleados.*;
import estacionamiento.*;
import asistencias.GestionAsistencias;
import facturacion.GestionPagos;
/**
 *
 * @author Danie
 */
public class Emergencia {

    private static Emergencia emergencia;
    
    private String nombre;
    private String telefono;
    private GestionChofer gestionchoferes;
    private GestionEnfermero gestionenfermeros;
    private GestionAdministrativos gestionadministrativo;
    private GestionDoctor gestiondoctor;
    private Estacionamiento estacionamiento;
    private GrupoFamiliar grupofamiliar;
    private GestionAsistencias gestionasistencias;
    private GestionPagos gestionpagos;
    

    private Emergencia(String nombre, String telefono) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.gestionchoferes = new GestionChofer();
        this.gestionenfermeros = new GestionEnfermero();
        this.gestionadministrativo = new GestionAdministrativos();
        this.gestiondoctor = new GestionDoctor();
        this.estacionamiento = new Estacionamiento();
        this.grupofamiliar = new GrupoFamiliar();
        this.gestionasistencias = new GestionAsistencias();
        this.gestionpagos = new GestionPagos();
    }

    
    
    
    public static Emergencia instancia(String nombre, String telefono){
        if(emergencia == null){
            emergencia = new Emergencia(nombre, telefono);
        }
        return emergencia;
    }

    public static Emergencia getEmergencia() {
        return emergencia;
    }

    public static void setEmergencia(Emergencia emergencia) {
        Emergencia.emergencia = emergencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public GestionChofer getGestionchoferes() {
        return gestionchoferes;
    }

    public void setGestionchoferes(GestionChofer gestionchoferes) {
        this.gestionchoferes = gestionchoferes;
    }

    public GestionEnfermero getGestionEnfermeros() {
        return gestionenfermeros;
    }

    public void setGestionenfermeros(GestionEnfermero gestionenfermeros) {
        this.gestionenfermeros = gestionenfermeros;
    }

    public GestionAdministrativos getGestionadministrativo() {
        return gestionadministrativo;
    }

    public void setGestionadministrativo(GestionAdministrativos gestionadministrativo) {
        this.gestionadministrativo = gestionadministrativo;
    }

    public GestionDoctor getGestiondoctor() {
        return gestiondoctor;
    }

    public void setGestiondoctor(GestionDoctor gestiondoctor) {
        this.gestiondoctor = gestiondoctor;
    }

    public Estacionamiento getEstacionamiento() {
        return estacionamiento;
    }

    public void setEstacionamiento(Estacionamiento estacionamiento) {
        this.estacionamiento = estacionamiento;
    }

    public GrupoFamiliar getGrupofamiliar() {
        return grupofamiliar;
    }

    public void setGrupofamiliar(GrupoFamiliar grupofamiliar) {
        this.grupofamiliar = grupofamiliar;
    }

    public GestionAsistencias getGestionasistencias() {
        return gestionasistencias;
    }

    public void setGestionasistencias(GestionAsistencias gestionasistencias) {
        this.gestionasistencias = gestionasistencias;
    }

    public GestionPagos getGestionpagos() {
        return gestionpagos;
    }

    public void setGestionpagos(GestionPagos gestionpagos) {
        this.gestionpagos = gestionpagos;
    }
    
    
}
