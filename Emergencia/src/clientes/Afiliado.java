/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientes;

import facturacion.FacturaPago;
import exceptions.GestionFamiliarException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Afiliado extends Persona{
    private String numeroDeAfiliado;
    private String plan;
    private Boolean estadoDeServicio;
    private ArrayList<Familiar> familiaresAfiliados = new ArrayList();
    
    public Afiliado(String numeroDeAfiliado, String plan, Boolean estadoDeServicio, String numeroDeFamilia, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(edad, nombre, apellido, numeroDeDocumento);
        this.numeroDeAfiliado = numeroDeAfiliado;
        this.plan = plan;
        this.estadoDeServicio = estadoDeServicio;
    }

    
    public Boolean getEstadoDeServicio() {
        return estadoDeServicio;
    }

    public void setEstadoDeServicio(Boolean estadoDeServicio) {
        this.estadoDeServicio = estadoDeServicio;
    }

    public String getNumeroDeAfiliado() {
        return numeroDeAfiliado;
    }

    public void setNumeroDeAfiliado(String numeroDeAfiliado) {
        this.numeroDeAfiliado = numeroDeAfiliado;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
    
    

    public ArrayList<Familiar> getFamiliaresAfiliados() {
        return familiaresAfiliados;
    }

    public void setFamiliaresAfiliados(ArrayList<Familiar> familiaresAfiliados) {
        this.familiaresAfiliados = familiaresAfiliados;
    }
    
    public void altaFamiliar(Familiar familiar) throws GestionFamiliarException{
        boolean band = false;
        for(Familiar f : familiaresAfiliados){
            if(f.getNumeroDeFamiliarDeAsociado().equals(familiar.getNumeroDeFamiliarDeAsociado()))
            {
                band=true;
            }   
        }
        if(!band){
            familiaresAfiliados.add(familiar);
        } else {
            throw new GestionFamiliarException("Familiar existente");
        }
    }
    

    public void bajaFamiliar(String numeroDeFamiliar) throws GestionFamiliarException{
        Boolean band = false;
        for(Familiar f : familiaresAfiliados){
             if (f.getNumeroDeFamiliarDeAsociado().equals(numeroDeFamiliar)){
                 familiaresAfiliados.remove(f);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionFamiliarException("No se encontró al Familiar");
         }
    }

    public Familiar buscarPorNumeroDeFamiliar(String numero) throws GestionFamiliarException {
        for (Familiar f : familiaresAfiliados) {
            if (f.getNumeroDeFamiliarDeAsociado().equals(numero)) {
                return f;
            }
        }
        throw new GestionFamiliarException("No se encontró al Familiar");
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Afiliado afiliado = (Afiliado) o;
        return Objects.equals(numeroDeAfiliado, afiliado.numeroDeAfiliado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numeroDeAfiliado);
    }
}
