/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientes;

import exceptions.GestionAfiliadoException;
import exceptions.GestionFamiliarException;
import java.util.ArrayList;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public class GrupoFamiliar implements GrupoFamiliarInterfaz{   

    private ArrayList<Afiliado> afiliados = new ArrayList();
    
    
    public ArrayList<Afiliado> getAfiliados() {
        return afiliados;
    }

    public void setAfiliados(ArrayList<Afiliado> afiliados) {
        this.afiliados = afiliados;
    }
    

    

    public void altaAfiliado(Afiliado afiliado) throws GestionAfiliadoException{
    boolean band = false;
    for(Afiliado a : afiliados){
        if(a.getNumeroDeAfiliado().equals(afiliado.getNumeroDeAfiliado()))
        {
            band=true;
        }   
    }
    if(!band){
        afiliados.add(afiliado);
    } else {
        throw new GestionAfiliadoException("Afiliado existente");
    }
}

    

    public void bajaAfiliado(String numeroDeAfiliado) throws GestionAfiliadoException{
        Boolean band = false;
        for(Afiliado a : afiliados){
             if (a.getNumeroDeAfiliado().equals(numeroDeAfiliado)){
                 afiliados.remove(a);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionAfiliadoException("No se encontró al Afiliado");
         }
    }

    public Afiliado buscarPorNumeroDeAfiliado(String numero) throws GestionAfiliadoException {
        for (Afiliado a : afiliados) {
            if (a.getNumeroDeAfiliado().equals(numero)) {
                return a;
            }
        }
        throw new GestionAfiliadoException("No se encontró al Afiliado");
    }
}
