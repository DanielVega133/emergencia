/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientes;

import asistencias.AsistenciaMedica;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Persona {
   private Integer edad;
    private String nombre;
    private String apellido;
    private String numeroDeDocumento;

    public Persona(Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        this.edad = edad;
        this.nombre = nombre;
        this.apellido = apellido;
        this.numeroDeDocumento = numeroDeDocumento;
    }

    

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNumeroDeDocumento() {
        return numeroDeDocumento;
    }

    public void setNumeroDeDocumento(String numeroDeDocumento) {
        this.numeroDeDocumento = numeroDeDocumento;
    }



    @Override
    public boolean equals(Object persona1) {
        //optimizacion
        if (this == persona1) {
            return true;
        }
        //no null
        if (persona1 == null) {
            return false;
        }
        //la clase es igual
        if (getClass() != persona1.getClass()) {
            return false;
        }
        //comparacion campo a campo
        Persona other = (Persona) persona1;
        if (other.getNombre().equals(this.nombre) && other.getApellido().equals(this.apellido) && other.getNumeroDeDocumento().equals(this.numeroDeDocumento)) {
            return true;
        }else{
            return false;
        }

    }
    
    
}
