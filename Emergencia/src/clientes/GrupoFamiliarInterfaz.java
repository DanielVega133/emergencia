/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package clientes;

import exceptions.GestionAfiliadoException;
import exceptions.GestionFamiliarException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GrupoFamiliarInterfaz{


    public ArrayList<Afiliado> getAfiliados();

    public void setAfiliados(ArrayList<Afiliado> afiliados);
    
   
    public void altaAfiliado(Afiliado afiliado) throws GestionAfiliadoException;

    public void bajaAfiliado(String numeroDeAfiliado) throws GestionAfiliadoException;

    public Afiliado buscarPorNumeroDeAfiliado(String numero) throws GestionAfiliadoException;
}