/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clientes;

import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Familiar extends Persona{
    private String numeroDeFamiliarDeAsociado;

    public Familiar(String numeroDeFamiliarDeAsociado, String numeroDeFamilia, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(edad, nombre, apellido, numeroDeDocumento);
        this.numeroDeFamiliarDeAsociado = numeroDeFamiliarDeAsociado;
    }


    public String getNumeroDeFamiliarDeAsociado() {
        return numeroDeFamiliarDeAsociado;
    }

    public void setNumeroDeFamiliarDeAsociado(String numeroDeFamiliarDeAsociado) {
        this.numeroDeFamiliarDeAsociado = numeroDeFamiliarDeAsociado;
    }
    
        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Familiar familiar = (Familiar) o;
        return Objects.equals(numeroDeFamiliarDeAsociado, familiar.numeroDeFamiliarDeAsociado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numeroDeFamiliarDeAsociado);
    }
}
