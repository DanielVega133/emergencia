/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package estacionamiento;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Auto {
    private String modelo;
    private String matricula;
    private boolean disponibilidad;

    public Auto(String modelo, String matricula, boolean disponibilidad) {
        this.modelo = modelo;
        this.matricula = matricula;
        this.disponibilidad = disponibilidad;
    }

    public boolean isDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auto auto = (Auto) o;
        return Objects.equals(matricula, auto.matricula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(matricula);
    }


}
