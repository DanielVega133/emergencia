/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package estacionamiento;

import exceptions.GestionAutoException; 
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface EstacionamientoInterfaz {
    public void altaAuto(Auto auto) throws GestionAutoException;
    
    public ArrayList<Auto> getAutos();

    public void bajaAuto(String matricula) throws GestionAutoException;

    public Auto buscarPorMatricula(String matricula) throws GestionAutoException;


}
