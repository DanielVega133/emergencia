/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package estacionamiento;

import exceptions.GestionAutoException;
import java.util.ArrayList;

/**
 *
 * @author Danie
 */
public class Estacionamiento implements EstacionamientoInterfaz{
    private ArrayList<Auto> autoList = new ArrayList<Auto>();


    public void altaAuto(Auto auto) throws GestionAutoException{
        boolean band = false;
        for(Auto a : autoList){
            if(a.getMatricula().equals(auto.getMatricula()))
            {
                band=true;
            }   
        }
        if(!band){
            autoList.add(auto);
        } else {
            throw new GestionAutoException("Auto existente");
        }
    }
    
    public ArrayList<Auto> getAutos() {
        return autoList;
    }

    public void bajaAuto(String matricula) throws GestionAutoException{
        Boolean band = false;
        for(Auto a : autoList){
             if (a.getMatricula().equals(matricula)){
                 autoList.remove(a);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionAutoException("No se encontró el Auto");
         }
    }

    public Auto buscarPorMatricula(String matricula) throws GestionAutoException {
        for (Auto a : autoList) {
            if (a.getMatricula().equals(matricula)) {
                return a;
            }
        }
        throw new GestionAutoException("No se encontró el Auto");
    }


}
