/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package interfaz;

import clientes.Afiliado;
import clientes.Familiar;
import emergencia.Emergencia;
import empleados.Administrativo;
import empleados.Chofer;
import empleados.Doctor;
import empleados.Enfermero;
import estacionamiento.Auto;
import exceptions.GestionAsistenciasException;
import asistencias.AsistenciaMedica;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Daniel
 */
public class VentanaNuevaAsistencia extends javax.swing.JFrame {

    Emergencia emergencia1 = Emergencia.instancia(null,null);
    private ValidarCampo validar= new ValidarCampo();
    

    public VentanaNuevaAsistencia() {
        initComponents();
        comboFamiliar.setEnabled(false);
        for(Administrativo admin : emergencia1.getGestionadministrativo().getAdministrativo()){
            if(admin.getDisponibilidad()==true){
                String nombre = null;
                String apellido = null;
                String completo = null;
                String matriculaAdmin = null;
                nombre = admin.getNombre();
                apellido = admin.getApellido();
                matriculaAdmin= admin.getNumeroDeEmpleadoAdministrativo();
                completo = nombre+"-"+apellido+"-"+matriculaAdmin;
                comboAdministrativo.addItem(completo);
            }
        }
        for(Chofer chofer : emergencia1.getGestionchoferes().getChoferes()){
            if(chofer.getDisponibilidad()==true){
                String nombre = null;
                String apellido = null;
                String completo = null;
                String licencia = null;
                nombre = chofer.getNombre();
                apellido = chofer.getApellido();
                licencia = chofer.getNumeroDeCarnet();
                System.out.println(licencia);
                completo = nombre+"-"+apellido+"-"+licencia;
                System.out.println(licencia);
                comboChofer.addItem(completo);
            }
            
        }

        for(Doctor doctor : emergencia1.getGestiondoctor().getDoctores()){
            if(doctor.getDisponibilidad()==true){
                String nombre = null;
                String apellido = null;
                String completo = null;
                String idMedico = null;
                nombre = doctor.getNombre();
                apellido = doctor.getApellido();
                idMedico = doctor.getNumeroLicenciaMedica();
                completo = nombre+"-"+apellido+"-"+idMedico;
                comboDoctor.addItem(completo);
            }
            
        }

        for(Enfermero enfermero : emergencia1.getGestionEnfermeros().getEnfermeros()){
            if(enfermero.getDisponibilidad()==true){
                String nombre = null;
                String apellido = null;
                String completo = null;
                String idEnfermero = null;
                nombre = enfermero.getNombre();
                apellido = enfermero.getApellido();
                idEnfermero = enfermero.getIdentificacionSanitaria();
                completo = nombre+"-"+apellido+"-"+idEnfermero;
                comboEnfermero.addItem(completo);
            }
            
        }

        for(Auto auto : emergencia1.getEstacionamiento().getAutos()){
            if(auto.isDisponibilidad()==true){
                String completo = null;
                String matricula = null;
                String modelo = null;
                modelo = auto.getModelo();
                matricula = auto.getMatricula();
                completo = modelo+"-"+matricula;
                comboAmbulancia.addItem(completo);
            }    
                
        }
        
        for(Afiliado afiliado : emergencia1.getGrupofamiliar().getAfiliados()){

                    if(afiliado.getEstadoDeServicio()==true){
                        String nombre = null;
                        String apellido = null;
                        String completo = null;
                        String idAfiliado = null;
                        nombre = afiliado.getNombre();
                        apellido = afiliado.getApellido();
                        idAfiliado = afiliado.getNumeroDeAfiliado();
                        completo = nombre+"-"+apellido+"-"+idAfiliado;
                        comboAfiliado.addItem(completo);
                    }
                }
            
        
        this.setResizable(false);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dia = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        descripcion = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        comboAmbulancia = new javax.swing.JComboBox<>();
        comboDoctor = new javax.swing.JComboBox<>();
        comboEnfermero = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        comboChofer = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        comboAfiliado = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        comboFamiliar = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        comboAdministrativo = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        anio = new javax.swing.JTextField();
        mes = new javax.swing.JComboBox<>();
        horaBox = new javax.swing.JComboBox<>();
        minutosBox = new javax.swing.JComboBox<>();
        buscar = new javax.swing.JButton();
        carga = new javax.swing.JButton();
        nroOperacion = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        dia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        jLabel8.setText("Fecha de asistencia:");

        jLabel12.setText("Hora de creacion de asistencia:");

        jLabel13.setText("hora:");

        jLabel14.setText("minutos");

        jLabel15.setText("describa brevemente la solicitud");

        jLabel5.setText("Doctor:");

        comboAmbulancia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAmbulanciaActionPerformed(evt);
            }
        });

        jLabel6.setText("Enfermero:");

        jLabel7.setText("Chofer:");

        jLabel9.setText("Año");

        jLabel10.setText("Mes");

        jLabel11.setText("Dia");

        jLabel1.setText("Afiliado solicitante");

        jLabel2.setText("En caso de que la asistencia sea a familiar marcar y rellenar la informacion");

        jRadioButton1.setText("Familiar");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jLabel3.setText("Administrativo que recibio la solicitud");

        jLabel4.setText("Ambulancia:");

        anio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anioActionPerformed(evt);
            }
        });

        mes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        mes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mesActionPerformed(evt);
            }
        });

        horaBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));

        minutosBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));

        buscar.setText("Buscar familiar");
        buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarActionPerformed(evt);
            }
        });

        carga.setText("Cargar asistencia");
        carga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargaActionPerformed(evt);
            }
        });

        jLabel17.setText("Nro operacion");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(comboAfiliado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel15)
                            .addComponent(descripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboAdministrativo, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nroOperacion)
                                .addGap(13, 13, 13)
                                .addComponent(carga))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel8)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(anio, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel10)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(dia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel12)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel5)
                                        .addComponent(jLabel6)
                                        .addComponent(jLabel7))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(comboEnfermero, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboChofer, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboDoctor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(comboAmbulancia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel13)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(horaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel14)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(minutosBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(comboFamiliar, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2))))
                .addGap(15, 15, 15))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buscar)
                    .addComponent(jLabel1)
                    .addComponent(comboAfiliado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboFamiliar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboAdministrativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(descripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(comboAmbulancia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(comboDoctor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboEnfermero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboChofer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(15, 15, 15)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(anio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(horaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(minutosBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(carga)
                    .addComponent(nroOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17))
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void anioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_anioActionPerformed

    private void mesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mesActionPerformed

    private void buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarActionPerformed
        
        String[] datosAfiliado = comboAfiliado.getSelectedItem().toString().split("-");
        for(Afiliado afiliado : emergencia1.getGrupofamiliar().getAfiliados()){
            if(afiliado.getNumeroDeAfiliado().equals(datosAfiliado[2])){
                for(Familiar familiar1 : afiliado.getFamiliaresAfiliados()){
                    String nombre = null;
                    String apellido = null;
                    String completo = null;
                    String idFamiliar = null;
                    nombre = familiar1.getNombre();
                    apellido = familiar1.getApellido();
                    idFamiliar = familiar1.getNumeroDeFamiliarDeAsociado();
                    completo = nombre+"-"+apellido+"-"+idFamiliar;
                    comboFamiliar.addItem(completo);
                }
            }
                
          }
          
        
            
    }//GEN-LAST:event_buscarActionPerformed

    private void cargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargaActionPerformed
        try {
            /*declaracion de las instancias*/
            Doctor doctor = new Doctor(null,null,null,null,null,null,null,null);
            Enfermero enfermero = new Enfermero(null, null, null, null, null,null,null);
            Chofer chofer = new Chofer(null, null, null, null,null,null,null);
            Administrativo administrativo = new Administrativo(null, null, null, null, null,null,null);
            Auto auto = new Auto(null, null,false);
            Familiar familiar = new Familiar(null, null, null, null,null,null);
            Afiliado afiliado = new Afiliado(null, null, null, null,null,null,null,null);
            Boolean familiarVerfic = false;
            
            String[] datosDoctor = comboDoctor.getSelectedItem().toString().split("-");
            for(Doctor doctor1 : emergencia1.getGestiondoctor().getDoctores()){
                if(doctor1.getNombre().equals(datosDoctor[0]) && doctor1.getApellido().equals(datosDoctor[1]) && doctor1.getIdentificacionLaboral().equals(datosDoctor[2])) {
                    doctor= doctor1;
                    doctor1.setDisponibilidad(false);
                }
            }
            
            String[] datosAdministrativo = comboAdministrativo.getSelectedItem().toString().split("-");
            for(Administrativo administrativo1 : emergencia1.getGestionadministrativo().getAdministrativo()){
                if((administrativo1.getNombre().equals(datosAdministrativo[0])) && (administrativo1.getApellido().equals(datosAdministrativo[1])) && (administrativo1.getNumeroDeEmpleadoAdministrativo().equals(datosAdministrativo[2]))){
                    administrativo= administrativo1;
                    administrativo1.setDisponibilidad(false);
                }
            }
            
            String[] datosChofer = comboChofer.getSelectedItem().toString().split("-");
            for(Chofer chofer1 : emergencia1.getGestionchoferes().getChoferes()){
                if((chofer1.getNombre().equals(datosChofer[0])) && (chofer1.getApellido().equals(datosChofer[1])) && (chofer1.getNumeroDeCarnet().equals(datosChofer[2]))){
                    chofer=chofer1;
                    chofer1.setDisponibilidad(false);
                }
            }
            
            String[] datosEnfermero = comboEnfermero.getSelectedItem().toString().split("-");
            for(Enfermero enfermero1 : emergencia1.getGestionEnfermeros().getEnfermeros()){
                if((enfermero1.getNombre().equals(datosEnfermero[0])) && (enfermero1.getApellido().equals(datosEnfermero[1])) && (enfermero1.getIdentificacionSanitaria().equals(datosEnfermero[2]))){
                    enfermero=enfermero1;
                    enfermero1.setDisponibilidad(false);
                }
            }
            
            String[] datosAuto = comboAmbulancia.getSelectedItem().toString().split("-");
            for(Auto auto1 : emergencia1.getEstacionamiento().getAutos()){
                if((auto1.getModelo().equals(datosAuto[0])) && (auto1.getMatricula().equals(datosAuto[1]))){
                    auto=auto1;
                    auto1.setDisponibilidad(false);
                }
            }
            
            String[] datosAfiliado = comboAfiliado.getSelectedItem().toString().split("-");
            for(Afiliado afiliado1 : emergencia1.getGrupofamiliar().getAfiliados()){
                if((afiliado1.getNombre().equals(datosAfiliado[0])) && (afiliado1.getApellido().equals(datosAfiliado[1])) && (afiliado1.getNumeroDeAfiliado().equals(datosAfiliado[2]))){
                    afiliado=afiliado1;
                    if(jRadioButton1.isSelected()==true){
                        String[] datosFamiliar = comboFamiliar.getSelectedItem().toString().split("-");
                        for(Familiar familiar1 : afiliado.getFamiliaresAfiliados()){
                            if((familiar1.getNombre().equals(datosFamiliar[0]))&&(familiar1.getApellido().equals(datosFamiliar[1]))&&(familiar1.getNumeroDeFamiliarDeAsociado().equals(datosFamiliar[2]))){
                            familiar=familiar1;
                            familiarVerfic=true;
                        }
                    }
                }
                }
            }
            
            
            
            
            String fecha = (anio.getText()+"-"+mes.getSelectedItem()+"-"+dia.getSelectedItem());
            LocalDate date = LocalDate.parse(fecha);
            int hora = Integer.parseInt(horaBox.getSelectedItem().toString());
            int minutos = Integer.parseInt(minutosBox.getSelectedItem().toString());
            LocalTime time = LocalTime.of(hora, minutos);
            String descripcionAsist=descripcion.getText();
            String numeroOperacion=nroOperacion.getText();
            AsistenciaMedica asistencia = new AsistenciaMedica(administrativo,chofer,doctor,enfermero,afiliado,familiar,auto,familiarVerfic,date,time,numeroOperacion,descripcionAsist);
            emergencia1.getGestionasistencias().altaAsistenciaMedica(asistencia);
            JOptionPane.showMessageDialog(this,"Asistencia cargada");
            anio.setText("");
            descripcion.setText("");
            nroOperacion.setText("");
        } catch (GestionAsistenciasException ex) {
            Logger.getLogger(VentanaNuevaAsistencia.class.getName()).log(Level.SEVERE, null, ex);
        }

                                                  

    
           
    }//GEN-LAST:event_cargaActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        if(jRadioButton1.isSelected()==true){
            comboFamiliar.setEnabled(true);
        }
        else{
            comboFamiliar.setEnabled(false);
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void comboAmbulanciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAmbulanciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboAmbulanciaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaNuevaAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaNuevaAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaNuevaAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaNuevaAsistencia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaNuevaAsistencia().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField anio;
    private javax.swing.JButton buscar;
    private javax.swing.JButton carga;
    private javax.swing.JComboBox<String> comboAdministrativo;
    private javax.swing.JComboBox<String> comboAfiliado;
    private javax.swing.JComboBox<String> comboAmbulancia;
    private javax.swing.JComboBox<String> comboChofer;
    private javax.swing.JComboBox<String> comboDoctor;
    private javax.swing.JComboBox<String> comboEnfermero;
    private javax.swing.JComboBox<String> comboFamiliar;
    private javax.swing.JTextField descripcion;
    private javax.swing.JComboBox<String> dia;
    private javax.swing.JComboBox<String> horaBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JComboBox<String> mes;
    private javax.swing.JComboBox<String> minutosBox;
    private javax.swing.JTextField nroOperacion;
    // End of variables declaration//GEN-END:variables
}
