/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package interfaz;
import empleados.*;
import emergencia.*;
import estacionamiento.Auto;
import exceptions.GestionAdministrativoException;
import exceptions.GestionChoferException;
import exceptions.GestionDoctorException;
import exceptions.GestionEnfermeroException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
/**
 *
 * @author Daniel
 */
public class VentanaAgregarEmpleados extends javax.swing.JFrame {
    Emergencia emergencia = Emergencia.instancia(null,null);
    private ValidarCampo validar= new ValidarCampo();
    /**
     * Creates new form VentanaEmpleados
     */
    public VentanaAgregarEmpleados() {
        initComponents(); // Llamar primero a initComponents
        identificacionSanitaria.setEnabled(false);
        especializacion.setEnabled(false);
        numeroLicenciaMedica.setEnabled(false);
        numeroDeCarnet.setEnabled(false);
        numeroDeEmpleadoAdministrativo.setEnabled(false);

        this.setResizable(false);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    }
    
    
    public boolean verificacion(){
        Boolean valor=false;
        for(Enfermero enfermero1 : emergencia.getGestionEnfermeros().getEnfermeros()){
            if(enfermero1.getIdentificacionLaboral().equals(matricula.getText())){
                valor=true;
                break;
            }
        }
        for (Doctor doctor1 : emergencia.getGestiondoctor().getDoctores()) {
            if (doctor1.getIdentificacionLaboral().equals(matricula.getText())) {
                valor = true;
                break;
            }
        }
        for (Chofer chofer1 : emergencia.getGestionchoferes().getChoferes()) {
            if (chofer1.getIdentificacionLaboral().equals(matricula.getText())) {
                valor = true;
                break;
            }   
        }
        for (Administrativo administrativo1 : emergencia.getGestionadministrativo().getAdministrativo()) {
            if (administrativo1.getIdentificacionLaboral().equals(matricula.getText())) {
                valor = true;
                break;
            }
        }for(Enfermero enfermero1 : emergencia.getGestionEnfermeros().getEnfermeros()){
            if(enfermero1.getIdentificacionSanitaria().equals(identificacionSanitaria.getText())){
                valor=true;
                break;
            }
        }
        for (Doctor doctor1 : emergencia.getGestiondoctor().getDoctores()) {
            if (doctor1.getNumeroLicenciaMedica().equals(numeroLicenciaMedica.getText())) {
                valor = true;
                break;
            }
        }
        for (Chofer chofer1 : emergencia.getGestionchoferes().getChoferes()) {
            if (chofer1.getNumeroDeCarnet().equals(numeroDeCarnet.getText())) {
                valor = true;
                break;
            }   
        }
        for (Administrativo administrativo1 : emergencia.getGestionadministrativo().getAdministrativo()) {
            if (administrativo1.getNumeroDeEmpleadoAdministrativo().equals(numeroDeEmpleadoAdministrativo.getText())) {
                valor = true;
                break;
            }
        }
        return valor;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        apellido = new javax.swing.JTextField();
        edad = new javax.swing.JTextField();
        dni = new javax.swing.JTextField();
        matricula = new javax.swing.JTextField();
        enfermeroboton = new javax.swing.JRadioButton();
        doctorboton = new javax.swing.JRadioButton();
        choferboton = new javax.swing.JRadioButton();
        administrativoboton = new javax.swing.JRadioButton();
        numeroDeEmpleadoAdministrativo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        numeroDeCarnet = new javax.swing.JTextField();
        especializacion = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        numeroLicenciaMedica = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        identificacionSanitaria = new javax.swing.JTextField();
        carga = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel2.setText("Rellene la informacion ");

        jLabel3.setText("nombre:");

        jLabel4.setText("apellido:");

        jLabel5.setText("edad:");

        jLabel6.setText("DNI:");

        jLabel7.setText("identificacion laboral");

        dni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dniActionPerformed(evt);
            }
        });

        enfermeroboton.setText("enfermero");
        enfermeroboton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enfermerobotonActionPerformed(evt);
            }
        });

        doctorboton.setText("doctor");
        doctorboton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                doctorbotonActionPerformed(evt);
            }
        });

        choferboton.setText("chofer");
        choferboton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choferbotonActionPerformed(evt);
            }
        });

        administrativoboton.setText("administrativo");
        administrativoboton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                administrativobotonActionPerformed(evt);
            }
        });

        jLabel8.setText("numero de empleado administrativo");

        jLabel9.setText("numero de carnet");

        numeroDeCarnet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numeroDeCarnetActionPerformed(evt);
            }
        });

        especializacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                especializacionActionPerformed(evt);
            }
        });

        jLabel10.setText("especializacion");

        jLabel11.setText("numero de licencia medica");

        jLabel12.setText("identificacion sanitaria");

        carga.setText("Cargar empleado");
        carga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(carga)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel2)
                        .addComponent(enfermeroboton)
                        .addComponent(doctorboton)
                        .addComponent(choferboton)
                        .addComponent(administrativoboton)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel5)
                                .addComponent(jLabel7))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(edad, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel6)
                                    .addGap(18, 18, 18)
                                    .addComponent(dni))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(32, 32, 32)
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(matricula)))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(identificacionSanitaria))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel10)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(especializacion, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jLabel11)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(numeroLicenciaMedica))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(numeroDeEmpleadoAdministrativo))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(numeroDeCarnet))))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(edad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(matricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(enfermeroboton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(identificacionSanitaria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(doctorboton)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(especializacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(numeroLicenciaMedica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(choferboton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(numeroDeCarnet, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(administrativoboton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(numeroDeEmpleadoAdministrativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(carga)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cargaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cargaActionPerformed
        if((nombre.getText().isEmpty()==false) && (apellido.getText().isEmpty()==false) && (edad.getText().isEmpty()==false) && (dni.getText().isEmpty()==false) && (matricula.getText().isEmpty()==false)){
            if(enfermeroboton.isSelected()==true ){
                if(verificacion()==false){
            try {
                Enfermero enfermero1 = new Enfermero(null,null,null,null,null,null,null);
                enfermero1.setApellido(apellido.getText());
                enfermero1.setNombre(nombre.getText());
                if(edad.getText().isEmpty()==false){
                    enfermero1.setEdad(Integer.parseInt(edad.getText()));
                }
                
                enfermero1.setIdentificacionLaboral(matricula.getText());
                enfermero1.setNumeroDeDocumento(dni.getText());
                enfermero1.setDisponibilidad(true);
                enfermero1.setIdentificacionSanitaria(identificacionSanitaria.getText());
                emergencia.getGestionEnfermeros().altaEnfermero(enfermero1);
                JOptionPane.showMessageDialog(this,"Empleado registrado");
                apellido.setText("");
                                dni.setText("");
                                edad.setText("");
                                especializacion.setText("");
                                identificacionSanitaria.setText("");
                                matricula.setText("");
                                nombre.setText("");
                                numeroDeCarnet.setText("");
                                numeroDeEmpleadoAdministrativo.setText("");
                                numeroLicenciaMedica.setText("");
                
            } catch (GestionEnfermeroException ex) {
                Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
                    JOptionPane.showMessageDialog(this,"verificar datos");
                }
            }
        else{
            if(choferboton.isSelected()==true) {
                if(verificacion()==false){
                try {
                     Chofer chofer1 = new Chofer(null, null, null, null, null,null,null);
                    chofer1.setApellido(apellido.getText());
                    chofer1.setNombre(nombre.getText());
                    if(edad.getText().isEmpty()==false){
                    chofer1.setEdad(Integer.parseInt(edad.getText()));
                    }
                    chofer1.setIdentificacionLaboral(matricula.getText());
                    chofer1.setNumeroDeDocumento(dni.getText());
                    chofer1.setNumeroDeCarnet(numeroDeCarnet.getText());
                    chofer1.setDisponibilidad(true);
                    emergencia.getGestionchoferes().altaChofer(chofer1);
                    JOptionPane.showMessageDialog(this,"Empleado registrado");
                    apellido.setText("");
                                dni.setText("");
                                edad.setText("");
                                especializacion.setText("");
                                identificacionSanitaria.setText("");
                                matricula.setText("");
                                nombre.setText("");
                                numeroDeCarnet.setText("");
                                numeroDeEmpleadoAdministrativo.setText("");
                                numeroLicenciaMedica.setText("");
                } catch (NumberFormatException ex) {
                        Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                } catch (GestionChoferException ex) {
                    Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                    JOptionPane.showMessageDialog(this,"verificar datos");
                }}
            else{
                if(doctorboton.isSelected()==true){
                    if(verificacion()==false){
                    try {
                        Doctor doctor1 = new Doctor(null, null, null, null, null, null,null,null);
                        doctor1.setApellido(apellido.getText());
                        doctor1.setNombre(nombre.getText());
                        if(edad.getText().isEmpty()==false){
                            doctor1.setEdad(Integer.parseInt(edad.getText()));
                        }
                        doctor1.setIdentificacionLaboral(matricula.getText());
                        doctor1.setNumeroDeDocumento(dni.getText());
                        doctor1.setTituloDeEspecializacion(especializacion.getText());
                        doctor1.setDisponibilidad(true);
                        doctor1.setNumeroLicenciaMedica(numeroLicenciaMedica.getText());
                        emergencia.getGestiondoctor().altaDoctor(doctor1);
                        JOptionPane.showMessageDialog(this,"Empleado registrado");
                        apellido.setText("");
                                dni.setText("");
                                edad.setText("");
                                especializacion.setText("");
                                identificacionSanitaria.setText("");
                                matricula.setText("");
                                nombre.setText("");
                                numeroDeCarnet.setText("");
                                numeroDeEmpleadoAdministrativo.setText("");
                                numeroLicenciaMedica.setText("");
                    }catch (NumberFormatException ex) {
                        Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (GestionDoctorException ex) {
                        Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    JOptionPane.showMessageDialog(this,"verificar datos");
                }}
                else
                {
                    if(administrativoboton.isSelected()==true){
                        if(verificacion()==false){
                         try {
                            Administrativo administrativo1 = new Administrativo(null,null, null, null, null, null,null);
                            administrativo1.setApellido(apellido.getText());
                            administrativo1.setNombre(nombre.getText());
                             if(edad.getText().isEmpty()==false){
                            administrativo1.setEdad(Integer.parseInt(edad.getText()));
                             }
                            administrativo1.setIdentificacionLaboral(matricula.getText());
                            administrativo1.setNumeroDeDocumento(dni.getText());
                            administrativo1.setDisponibilidad(true);
                            administrativo1.setNumeroDeEmpleadoAdministrativo(numeroDeEmpleadoAdministrativo.getText());
                            emergencia.getGestionadministrativo().altaAdministrativo(administrativo1);
                            JOptionPane.showMessageDialog(this,"Empleado registrado");
                                apellido.setText("");
                                dni.setText("");
                                edad.setText("");
                                especializacion.setText("");
                                identificacionSanitaria.setText("");
                                matricula.setText("");
                                nombre.setText("");
                                numeroDeCarnet.setText("");
                                numeroDeEmpleadoAdministrativo.setText("");
                                numeroLicenciaMedica.setText("");
                    }catch (NumberFormatException ex) {
                        Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (GestionAdministrativoException ex) {
                        Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    }else{
                    JOptionPane.showMessageDialog(this,"verificar datos");
                }}

                }
            }
        }
        
    }
       
    }//GEN-LAST:event_cargaActionPerformed

    private void enfermerobotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enfermerobotonActionPerformed
        if(enfermeroboton.isSelected()==true){
            doctorboton.setSelected(false);
            choferboton.setSelected(false);
            administrativoboton.setSelected(false);
            identificacionSanitaria.setEnabled(true);
            especializacion.setEnabled(false);
            numeroLicenciaMedica.setEnabled(false);
            numeroDeCarnet.setEnabled(false);
            numeroDeEmpleadoAdministrativo.setEnabled(false);
        }
        else{
            identificacionSanitaria.setEnabled(false);
        }     
    }//GEN-LAST:event_enfermerobotonActionPerformed

    private void especializacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_especializacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_especializacionActionPerformed

    private void numeroDeCarnetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numeroDeCarnetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numeroDeCarnetActionPerformed

    private void doctorbotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_doctorbotonActionPerformed
        if(doctorboton.isSelected()==true){
            enfermeroboton.setSelected(false);
            choferboton.setSelected(false);
            administrativoboton.setSelected(false);
            identificacionSanitaria.setEnabled(false);
            especializacion.setEnabled(true);
            numeroLicenciaMedica.setEnabled(true);
            numeroDeCarnet.setEnabled(false);
            numeroDeEmpleadoAdministrativo.setEnabled(false);
        }
        else{
            especializacion.setEnabled(false);
            numeroLicenciaMedica.setEnabled(false);
        }
    }//GEN-LAST:event_doctorbotonActionPerformed

    private void choferbotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choferbotonActionPerformed
        if(choferboton.isSelected()==true){
            enfermeroboton.setSelected(false);
            doctorboton.setSelected(false);
            administrativoboton.setSelected(false);
            identificacionSanitaria.setEnabled(false);
            especializacion.setEnabled(false);
            numeroLicenciaMedica.setEnabled(false);
            numeroDeCarnet.setEnabled(true);
            numeroDeEmpleadoAdministrativo.setEnabled(false);
        }else
        {
           numeroDeCarnet.setEnabled(false); 
        }
    }//GEN-LAST:event_choferbotonActionPerformed

    private void administrativobotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_administrativobotonActionPerformed
        if(administrativoboton.isSelected()==true){
            enfermeroboton.setSelected(false);
            doctorboton.setSelected(false);
            choferboton.setSelected(false);
            identificacionSanitaria.setEnabled(false);
            especializacion.setEnabled(false);
            numeroLicenciaMedica.setEnabled(false);
            numeroDeCarnet.setEnabled(false);
            numeroDeEmpleadoAdministrativo.setEnabled(true);
        }
        else{
            numeroDeEmpleadoAdministrativo.setEnabled(false);
        }
    }//GEN-LAST:event_administrativobotonActionPerformed

    private void dniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dniActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dniActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaAgregarEmpleados.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaAgregarEmpleados().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton administrativoboton;
    private javax.swing.JTextField apellido;
    private javax.swing.JButton carga;
    private javax.swing.JRadioButton choferboton;
    private javax.swing.JTextField dni;
    private javax.swing.JRadioButton doctorboton;
    private javax.swing.JTextField edad;
    private javax.swing.JRadioButton enfermeroboton;
    private javax.swing.JTextField especializacion;
    private javax.swing.JTextField identificacionSanitaria;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField matricula;
    private javax.swing.JTextField nombre;
    private javax.swing.JTextField numeroDeCarnet;
    private javax.swing.JTextField numeroDeEmpleadoAdministrativo;
    private javax.swing.JTextField numeroLicenciaMedica;
    // End of variables declaration//GEN-END:variables
}
