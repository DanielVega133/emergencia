/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package interfaz;

import exceptions.CampoVacioException;

/**
 *
 * @author Daniel
 */
public class ValidarCampo {
        public void detectarCampoVacio(String detector) throws CampoVacioException{
        if(detector.trim().equals("")){
           throw new CampoVacioException();
        }
    }
    
}
