/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package empleados;

import exceptions.GestionChoferException; 
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionChoferInterfaz {
    public void altaChofer(Chofer chofer) throws GestionChoferException;
    public ArrayList<Chofer> getChoferes();
    public void bajaChofer(String idChofer) throws GestionChoferException;
    public Chofer buscarPorCarnet(String id) throws GestionChoferException;
}
