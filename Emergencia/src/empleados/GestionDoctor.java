/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import exceptions.GestionDoctorException;
import java.util.ArrayList;
/**
 *
 * @author Daniel
 */
public class GestionDoctor implements GestionDoctorInterfaz{
    private ArrayList<Doctor> doctorList = new ArrayList<Doctor>();
    
    public void altaDoctor(Doctor doctor) throws GestionDoctorException{
        boolean band = false;
        for(Doctor doc : doctorList){
            if(doc.getNumeroLicenciaMedica().equals(doctor.getNumeroLicenciaMedica()))
            {
                band=true;
            }   
        }
        if(!band){
            doctorList.add(doctor);
        } else {
            throw new GestionDoctorException("Doctor existente");
        }
    }
    
    public ArrayList<Doctor> getDoctores() {
        return doctorList;
    }

    public void bajaDoctor(String matriculaDeDoctor) throws GestionDoctorException{
        Boolean band = false;
        for(Doctor doc : doctorList){
             if (doc.getNumeroLicenciaMedica().equals(matriculaDeDoctor)){
                 doctorList.remove(doc);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionDoctorException("No se encontró al Doctor");
         }
    }

    public Doctor buscarPorNumeroLicenciaMedica(String matricula) throws GestionDoctorException {
        for (Doctor doc : doctorList) {
            if (doc.getNumeroLicenciaMedica().equals(matricula)) {
                return doc;
            }
        }
        throw new GestionDoctorException("No se encontró al Doctor");
    }
}
