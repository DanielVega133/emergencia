/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package empleados;

import exceptions.GestionAdministrativoException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionAdministrativosInterfaz {
    public void altaAdministrativo(Administrativo admin) throws GestionAdministrativoException;
    public ArrayList<Administrativo> getAdministrativo();
    public void bajaAdmin(String numeroDeEmpleadoAdmin) throws GestionAdministrativoException;
    public Administrativo buscarPorNumeroDeEmpleado( String numero) throws GestionAdministrativoException;
}
