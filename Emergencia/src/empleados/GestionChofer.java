/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import exceptions.GestionChoferException;
import java.util.ArrayList;
import exceptions.GestionChoferException;
/**
 *
 * @author Daniel
 */
public class GestionChofer implements GestionChoferInterfaz{
    private ArrayList<Chofer> choferList = new ArrayList<Chofer>();

    public void altaChofer(Chofer chofer) throws GestionChoferException {
        boolean band = false;
        for (Chofer chofer1 : choferList) {
            if (chofer1.getNumeroDeCarnet().equals(chofer.getNumeroDeCarnet())) {
                band = true;
                break;
            }
        }
        if (!band) {
            choferList.add(chofer);
        } else {
            throw new GestionChoferException("Chofer existente");
        }
    }

    public ArrayList<Chofer> getChoferes() {
        return choferList;
    }

    public void bajaChofer(String numeroDeCarnetABorrar) throws GestionChoferException {
        boolean band = false;
        for (Chofer chofer1 : choferList) {
            if (chofer1.getNumeroDeCarnet().equals(numeroDeCarnetABorrar)) {
                choferList.remove(chofer1);
                band = true;
                break;
            }
        }
        if (!band) {
            throw new GestionChoferException("No se encontró el Chofer");
        }
    }

    public Chofer buscarPorCarnet(String numeroDeCarnetBuscado) throws GestionChoferException {
        for (Chofer chofer : choferList) {
            if (chofer.getNumeroDeCarnet().equals(numeroDeCarnetBuscado)) {
                return chofer;
            }
        }
        throw new GestionChoferException("No se encontró el Chofer");
    }
}
