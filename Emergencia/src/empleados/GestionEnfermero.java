/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;
import exceptions.GestionEnfermeroException;
import java.util.*;
/**
 *
 * @author Daniel
 */
public class GestionEnfermero implements GestionEnfermeroInterfaz{
    private ArrayList<Enfermero> enfermeroList = new ArrayList<Enfermero>();
    
    public void altaEnfermero(Enfermero enfermero) throws GestionEnfermeroException{
        boolean band = false;
        for(Enfermero enf : enfermeroList){
            if(enf.getIdentificacionSanitaria().equals(enfermero.getIdentificacionSanitaria()))
            {
                band=true;
            }   
        }
        if(!band){
            enfermeroList.add(enfermero);
        } else {
            throw new GestionEnfermeroException("Enfermero existente");
        }
    }
    
    public ArrayList<Enfermero> getEnfermeros() {
        return enfermeroList;
    }

    public void bajaEnfermero(String matriculaDeEnfermero) throws GestionEnfermeroException{
        Boolean band = false;
        for(Enfermero enf : enfermeroList){
             if (enf.getIdentificacionSanitaria().equals(matriculaDeEnfermero)){
                 enfermeroList.remove(enf);
                 band = true;
                 break;
           }
         }
         if(!band){
            throw new GestionEnfermeroException("No se encontró al Enfermero");
         }
    }

    public Enfermero buscarPorIdentificacionSanitaria(String matricula) throws GestionEnfermeroException {
        for (Enfermero enf : enfermeroList) {
            if (enf.getIdentificacionSanitaria().equals(matricula)) {
                return enf;
            }
        }
        throw new GestionEnfermeroException("No se encontró al Enfermero");
    }
}
