/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package empleados;

import exceptions.GestionDoctorException; 
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionDoctorInterfaz {
    public void altaDoctor(Doctor doctor) throws GestionDoctorException;
    public ArrayList<Doctor> getDoctores();
    public void bajaDoctor(String matriculaDeDoctor) throws GestionDoctorException;
    public Doctor buscarPorNumeroLicenciaMedica(String matricula) throws GestionDoctorException;
}
