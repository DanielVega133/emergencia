/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import exceptions.GestionAdministrativoException;
import java.util.ArrayList;
/**
 *
 * @author Daniel
 */
public class GestionAdministrativos implements GestionAdministrativosInterfaz{
    private ArrayList<Administrativo> administrativoList = new ArrayList<Administrativo>();
    
    
    public void altaAdministrativo(Administrativo admin) throws GestionAdministrativoException{
        boolean band = false;
        for(Administrativo admin1 : administrativoList){
            if(admin1.getNumeroDeEmpleadoAdministrativo().equals(admin.getNumeroDeEmpleadoAdministrativo()))
            {
                band=true;
            }   
        }
        if(band==false){
        administrativoList.add(admin);
        }else{
            throw new GestionAdministrativoException("Administrativo existente");
        }

    }
    
    public ArrayList<Administrativo> getAdministrativo() {
        return administrativoList;
    }

    public void bajaAdmin(String numeroDeEmpleadoAdmin) throws GestionAdministrativoException{
        Boolean band = false;
        for(Administrativo admin1 : administrativoList){
             if (admin1.getNumeroDeEmpleadoAdministrativo().equals(numeroDeEmpleadoAdmin)){
                 administrativoList.remove(admin1);
                 band = true;
                 break;
           }
         }
         if(band == false){
            throw new GestionAdministrativoException("No se encontro el Administrativo");
         }
    }


    public Administrativo buscarPorNumeroDeEmpleado( String numero) throws GestionAdministrativoException{
        boolean band = false;
        for (Administrativo admin : administrativoList) {
            if (admin.getNumeroDeEmpleadoAdministrativo().equals(numero)) {
                band = true;
                return admin;
            }
        }
         if(band == false){
            throw new GestionAdministrativoException("No se encontro el Administrativo");
         }
        return null;
    }
}
