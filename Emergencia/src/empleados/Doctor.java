/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Doctor extends Empleado{
    private String tituloDeEspecializacion;
    private String numeroLicenciaMedica;

    public Doctor(String tituloDeEspecializacion, String numeroLicenciaMedica, String identificacionLaboral, Boolean disponibilidad, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(identificacionLaboral, disponibilidad, edad, nombre, apellido, numeroDeDocumento);
        this.tituloDeEspecializacion = tituloDeEspecializacion;
        this.numeroLicenciaMedica = numeroLicenciaMedica;
    }

    public String getTituloDeEspecializacion() {
        return tituloDeEspecializacion;
    }

    public void setTituloDeEspecializacion(String tituloDeEspecializacion) {
        this.tituloDeEspecializacion = tituloDeEspecializacion;
    }

    public String getNumeroLicenciaMedica() {
        return numeroLicenciaMedica;
    }

    public void setNumeroLicenciaMedica(String numeroLicenciaMedica) {
        this.numeroLicenciaMedica = numeroLicenciaMedica;
    }


    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(numeroLicenciaMedica, doctor.numeroLicenciaMedica);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numeroLicenciaMedica);
    }
}
