/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Enfermero extends Empleado{
    private String identificacionSanitaria;

    public Enfermero(String identificacionSanitaria, String identificacionLaboral, Boolean disponibilidad, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(identificacionLaboral, disponibilidad, edad, nombre, apellido, numeroDeDocumento);
        this.identificacionSanitaria = identificacionSanitaria;
    }

    public String getIdentificacionSanitaria() {
        return identificacionSanitaria;
    }

    public void setIdentificacionSanitaria(String identificacionSanitaria) {
        this.identificacionSanitaria = identificacionSanitaria;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Enfermero enfermero = (Enfermero) o;
        return Objects.equals(identificacionSanitaria, enfermero.identificacionSanitaria);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identificacionSanitaria);
    }

    
}
