/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Administrativo extends Empleado {
    private String numeroDeEmpleadoAdministrativo;
    
    

    public Administrativo(String numeroDeEmpleadoAdministrativo, String identificacionLaboral, Boolean disponibilidad, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(identificacionLaboral, disponibilidad, edad, nombre, apellido, numeroDeDocumento);
        this.numeroDeEmpleadoAdministrativo = numeroDeEmpleadoAdministrativo;
    }

    

   


    public String getNumeroDeEmpleadoAdministrativo() {
        return numeroDeEmpleadoAdministrativo;
    }

    public void setNumeroDeEmpleadoAdministrativo(String numeroDeEmpleadoAdmin) {
        this.numeroDeEmpleadoAdministrativo = numeroDeEmpleadoAdministrativo;
    }
    
        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Administrativo that = (Administrativo) o;
        return Objects.equals(numeroDeEmpleadoAdministrativo, that.numeroDeEmpleadoAdministrativo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numeroDeEmpleadoAdministrativo);
    }
}
