/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package empleados;

import exceptions.GestionEnfermeroException;
import java.util.ArrayList;

/**
 *
 * @author Daniel
 */
public interface GestionEnfermeroInterfaz {
    public void altaEnfermero(Enfermero enfermero) throws GestionEnfermeroException;
    
    public ArrayList<Enfermero> getEnfermeros();

    public void bajaEnfermero(String matriculaDeEnfermero) throws GestionEnfermeroException;

    public Enfermero buscarPorIdentificacionSanitaria(String matricula) throws GestionEnfermeroException;
}
