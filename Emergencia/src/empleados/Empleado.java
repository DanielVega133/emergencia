/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import clientes.Persona;
import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Empleado extends Persona{
    private String identificacionLaboral;
    private Boolean disponibilidad;

    public Empleado(String identificacionLaboral, Boolean disponibilidad, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(edad, nombre, apellido, numeroDeDocumento);
        this.identificacionLaboral = identificacionLaboral;
        this.disponibilidad = disponibilidad;
    }

    public String getIdentificacionLaboral() {
        return identificacionLaboral;
    }

    public void setIdentificacionLaboral(String identificacionLaboral) {
        this.identificacionLaboral = identificacionLaboral;
    }

    public Boolean getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(Boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Empleado empleado = (Empleado) o;
        return Objects.equals(identificacionLaboral, empleado.identificacionLaboral);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identificacionLaboral);
    }

}
