/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package empleados;

import estacionamiento.Auto;
import java.util.Objects;

/**
 *
 * @author Danie
 */
public class Chofer extends Empleado{
    private String numeroDeCarnet;

    public Chofer(String numeroDeCarnet, String identificacionLaboral, Boolean disponibilidad, Integer edad, String nombre, String apellido, String numeroDeDocumento) {
        super(identificacionLaboral, disponibilidad, edad, nombre, apellido, numeroDeDocumento);
        this.numeroDeCarnet = numeroDeCarnet;
    }

    
    public String getNumeroDeCarnet() {
        return numeroDeCarnet;
    }

    public void setNumeroDeCarnet(String numeroDeCarnet) {
        this.numeroDeCarnet = numeroDeCarnet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Chofer chofer = (Chofer) o;
        return Objects.equals(numeroDeCarnet, chofer.numeroDeCarnet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numeroDeCarnet);
    }

    
}
